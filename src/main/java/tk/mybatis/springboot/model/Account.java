package tk.mybatis.springboot.model;

/**
 * 用户信息
 *
 * @author liuzh
 * @since 2016-01-31 21:39
 */
public class Account extends BaseEntity {
    private String username;
    private String password;
    private Double balance;
    private String status;
    private String role;

    public Account() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
