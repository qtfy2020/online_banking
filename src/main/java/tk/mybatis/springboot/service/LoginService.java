/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 abel533@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package tk.mybatis.springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.springboot.mapper.LoginMapper;
import tk.mybatis.springboot.mapper.PersoninfoMapper;
import tk.mybatis.springboot.model.Account;
import tk.mybatis.springboot.model.Person;

/**
 * @author liuzh
 * @since 2015-12-19 11:09
 */
@Service
public class LoginService {

    @Autowired
    private LoginMapper loginMapper;
    @Autowired
    private PersoninfoMapper personinfoMapper;

    public Account findByUser(Account userInfo) throws  Exception{
        Account account=loginMapper.selectOne(userInfo);
        if(account==null){
            throw new Exception("用户名或密码错误");
        }
        if ("冻结".equalsIgnoreCase(account.getStatus())){
            throw new Exception("该账户已被冻结");
        }
        /*if (!account.getRole().equalsIgnoreCase(userInfo.getRole())){
            throw new Exception("角色错误");
        }*/
        return account;
    }

    public Integer changeUser(Account account) {
        return  loginMapper.updateByPrimaryKey(account);
    }

    public Person findByPerson(Person person) throws Exception {
        Person persons=personinfoMapper.selectOne(person);
        if (persons!=null) {
            return persons;
        }else{
            throw new Exception("找不到该用户信息");
        }
    }

    public int updatePerson(Person person){
        return  personinfoMapper.updateByPrimaryKey(person);
    }
}
