package tk.mybatis.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import tk.mybatis.springboot.model.Account;
import tk.mybatis.springboot.model.Menu;
import tk.mybatis.springboot.model.MenuVo;
import tk.mybatis.springboot.service.MenuService;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author liuzh
 * @since 2015-12-19 11:10
 */
@Controller
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @RequestMapping
    @ResponseBody
    public List<MenuVo> getAll(Menu menu, HttpSession session) {
        Account account=(Account) session.getAttribute("account");
        List<MenuVo> menuList = menuService.getAll(menu,account);
        return menuList;
    }
}
