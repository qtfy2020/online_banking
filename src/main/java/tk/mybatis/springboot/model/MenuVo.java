package tk.mybatis.springboot.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;


public class MenuVo {
    private String id;
    private String name;
    private String action;
    @JsonProperty("iconCls")
    private String icon;
    private Boolean isParent;
    private String parentId;
    private Boolean checked;
    private Boolean open;
    private String menutype;
    private List<MenuVo> children;

    public MenuVo() {
    }

    public MenuVo(Menu menu) {
        this.id = menu.getId();
        this.name = menu.getName();
        this.action = menu.getAction();
        this.icon = menu.getIcon();
        this.menutype=menu.getMenutype();
        if (menu.getChecked()==0) {
            this.checked = true;
        }else {
            this.checked=false;
        }
        if (menu.getIsParent()==0) {
            this.isParent= true;
        }else {
            this.isParent=false;
        }
        if (menu.getOpen()==0) {
            this.open = true;
        }else {
            this.open=false;
        }
        this.parentId = menu.getParentId();
        this.children=new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getParent() {
        return isParent;
    }

    public void setParent(Boolean parent) {
        isParent = parent;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Boolean getOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    public List<MenuVo> getChildren() {
        return children;
    }

    public void setChildren(List<MenuVo> children) {
        this.children = children;
    }

    public String getMenutype() {
        return menutype;
    }

    public void setMenutype(String menutype) {
        this.menutype = menutype;
    }
}
