# Spring Boot 集成 MyBatis, 分页插件 PageHelper, 通用 Mapper 

运行项目:
    http://localhost:9090/
账号: admin         密码：123456   管理员
账号：qinjianping   密码：123456   普通用户

导出代码->在idea或eclipse中打开->在resources下找到test.sql和application-dev.properties文件，根据自己的配置修改相应的数据库配置，导入数据
然后运行Application


<br/>

**演示效果图：**
<br/>
1.登录页面
![管理员登录页面](https://gitee.com/zhongqiaoli/online_banking/raw/master/bank/1.png)
![普通用户登录页面](https://gitee.com/zhongqiaoli/online_banking/raw/master/bank/5.png)
<br/>
2.首页
![首页](https://gitee.com/zhongqiaoli/online_banking/raw/master/bank/2.png)
![首页](https://gitee.com/zhongqiaoli/online_banking/raw/master/bank/6.png)
<br/>
3.账户信息---所有账户
![所有账户](https://gitee.com/zhongqiaoli/online_banking/raw/master/bank/3.png)
<br/>
4.锁屏
![锁屏](https://gitee.com/zhongqiaoli/online_banking/raw/master/bank/4.png)
<br/>
5.银行业务---交易记录
![交易记录](https://gitee.com/zhongqiaoli/online_banking/raw/master/bank/7.png)
