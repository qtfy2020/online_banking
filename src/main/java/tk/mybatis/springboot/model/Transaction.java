
package tk.mybatis.springboot.model;

import java.util.Date;

public class Transaction extends BaseEntity {
    private String accountid;
    private String otherid;
    private Double trMoney;
    private Date datetime;
    private String type;

    public Transaction() {
    }

    public String getAccountid() {
        return accountid;
    }

    public void setAccountid(String accountid) {
        this.accountid = accountid;
    }

    public String getOtherid() {
        return otherid;
    }

    public void setOtherid(String otherid) {
        this.otherid = otherid;
    }

    public Double getTrMoney() {
        return trMoney;
    }

    public void setTrMoney(Double trMoney) {
        this.trMoney = trMoney;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "accountid='" + accountid + '\'' +
                ", otherid='" + otherid + '\'' +
                ", trMoney=" + trMoney +
                ", datetime=" + datetime +
                ", type='" + type + '\'' +
                '}';
    }
}
